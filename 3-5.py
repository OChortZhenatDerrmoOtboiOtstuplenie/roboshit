import cv2 as cv
import numpy as np

img_path = 'imgs/test{}.jpg'
read_mode = cv.IMREAD_GRAYSCALE
base_dp = 2
dist = 20000
dp = base_dp

for i in (3, 5):

    img = cv.imread(img_path.format(i), read_mode)
    if img is None:
        raise RuntimeError()
  
    new_img = cv.imread(img_path.format(i), cv.IMREAD_COLOR)
    img_copy = cv.blur(img.copy(), (7, 8))
    circles = cv.HoughCircles(img_copy, cv.HOUGH_GRADIENT, dp, dist)

    while circles is None:
        dp += 0.1
        circles = cv.HoughCircles(img_copy, cv.HOUGH_GRADIENT, dp, dist)
    circles = np.round(circles[0, :]).astype('int')

    for (x, y, r) in circles:
        cv.circle(new_img, (x, y), r, (0, 255, 0), 2)
    
    cv.imshow('output', new_img)
    cv.waitKey(0)
    dp = base_dp