import cv2 as cv
import numpy as np

img_path = 'imgs/test{}.jpg' # 6 - max
read_mode = cv.IMREAD_GRAYSCALE
base_dp = 2 # 1.7 [1, 2, 4]
dist = 20000 # to fing only one possible circle
dp = base_dp

for i in range(1, 6): # for each example
    img = cv.imread(img_path.format(i), read_mode)
    if img is None: # if couldn't read image dies
        raise RuntimeError()

    new_img = cv.imread(img_path.format(i), cv.IMREAD_COLOR) # image for result
    img_copy = cv.blur(img.copy(), (7, 8)) # blurs work copy of current example (8,8) [1, 2, 4, 5]; (7,7) [1, 2, 3, 4]; (7,8) [1, 2, 3, 4, 5]
    circles = cv.HoughCircles(img_copy, cv.HOUGH_GRADIENT, dp, dist) # searching for circles

    while circles is None: # if no circles found, change dp
        dp += 0.1
        circles = cv.HoughCircles(img_copy, cv.HOUGH_GRADIENT, dp, dist)
    circles = np.round(circles[0, :]).astype('int')

    for (x, y, r) in circles: # printing all found circles
        cv.circle(new_img, (x, y), r, (0, 255, 0), 2)
    
    cv.imshow('output', new_img)
    cv.waitKey(0)
    dp = base_dp # returning to default dp