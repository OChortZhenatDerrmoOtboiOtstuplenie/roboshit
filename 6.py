import cv2 as cv
import numpy as np

img_path = 'imgs/test6.jpg'
read_mode = cv.IMREAD_GRAYSCALE
base_dp = 1
dist = 20000
dp = base_dp

img = cv.imread(img_path, read_mode)
if img is None:
    raise RuntimeError()

new_img = cv.imread(img_path, cv.IMREAD_COLOR)
img_copy = img.copy()
circles = cv.HoughCircles(img_copy, cv.HOUGH_GRADIENT, dp, dist)

while circles is None:
    dp += 0.1
    circles = cv.HoughCircles(img_copy, cv.HOUGH_GRADIENT, dp, dist)
circles = np.round(circles[0, :]).astype('int')

for (x, y, r) in circles:
    cv.circle(new_img, (x, y), r, (0, 255, 0), 2)

cv.imshow('output', new_img)
cv.waitKey(0)